import { Component } from '@angular/core';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {
  title = 'assignment';
  public eventsList: any = [];
  public loadingEvents: boolean = true;
  constructor(private appService: AppService) {}

  ngOnInit() {
    	this.getEvents();
  }

   getEvents(){
  		this.appService.getToken().subscribe((data) => {
    	var token = data.access_token;
     	this.appService.getList(token).subscribe((list) => {
     		this.eventsList = list.items;
     		this.loadingEvents = false;		
    	});
    },(err) => {
              if(err.error.Status !== 200){
                alert('Please check for APIs details');
              }
            });     	
  }

   addEvents(form){
   	var title = form.value.name;
    var start = form.value.start;
    var end = form.value.end;
    this.loadingEvents = false;
  	this.appService.getToken().subscribe((data) => {
  		var token = data.access_token;
 		this.appService.addEvent(token,title,start,end).subscribe((list) => {
 			this.eventsList = list.items;
 			form.resetForm(); 
 			this.getEvents();
		});    	
    },(err) => {
              if(err.error.Status !== 200){
                alert('Please check for APIs details');
              }
            });   
  }
}