import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AppService {

    constructor(private http: HttpClient) { }


 	getToken() : Observable<any> {
       let headers = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});
       return this.http.post(environment.authUrl,{
        "client_id":environment.clientId,
        "client_secret":environment.clientSecret,
        "redirect_uri":environment.redirectUri,
        'grant_type': "authorization_code",
         'code' : environment.codeString
       },{headers})
        .pipe(
          map((response: Response) => {
            return response;
          }));
    }

    getList(token) : Observable<any> {
      let headers = new HttpHeaders({'Authorization': 'Bearer '+token });      
        return this.http.get('https://www.googleapis.com/calendar/v3/calendars/'+environment.calenderId+'/events',{headers})
          .pipe(
              map((response: Response) => {
                return response;
        }));
    }

    addEvent(token,title,start,end) : Observable<any> {
      let headers = new HttpHeaders({'Authorization': 'Bearer '+token });
      
        return this.http.post('https://www.googleapis.com/calendar/v3/calendars/'+environment.calenderId+'/events',
        {
          'start' : {'dateTime' :start},
          'end' : {'dateTime':end},
          'summary' : title,
        },{headers})
          .pipe(
              map((response: Response) => {
                return response;
        }));
    }

}
